The project aims at fostering our understanding of how the user experiences and copes with spatial and semantic uncertainties of landmark representations map displays based on Volunteered Geographic Information (VGI) for spatial orientation, wayfinding and navigation tasks. 

Spatial uncertainty is caused by potential spatial inaccuracies of map elements. Especially maps based on VGI are prone to variances in data quality (Girres & Touya, 2010; van Exel et al., 2010). Reasons for spatial inaccuracies in the context of VGI-based maps are the varying expertise and motivation of map data contributors, as well as the use of different devices for georeferencing and environmental factors such as the availability of GPS signals with sufficient signal strength (Sieber & Haklay, 2015; Zandbergen & Barbeau, 2011). Consequentially, landmark representations in maps can be located in the wrong map location with varying degrees of inaccuracy. In this context, spatial uncertainty is the uncertainty whether a specific landmark representation is correctly georeferenced within a map. Semantic uncertainty on the other hand is caused by potentially inaccurate assignments of semantic characteristics, e.g. by representing a landmark with an inappropriate landmark pictogram. 

![Spatial and semantic inaccuracies of landmark representations in the map](Fig_1-_landmark_inaccuracies.png)

**Fig. 1. Spatial and semantic inaccuracies of landmark representations in the map.** *The landmark representation in the left map is spatially inaccurate, as it has been placed in the wrong map location (too far down the road). The landmark representation in the right map is semantically inaccurate, as the semantic information conveyed by the pictogram (gas station) does not fit to the represented landmark (recycling containers).* 

In several studies, we examine how landmark uncertainty affects map matching and, consequentially, wayfinding and pedestrian navigation performance of a user, and how cognitive principles and cartographic design can be used to overcome expected biases related to the characteristics of VGI data. Based on a series of experimental studies, empirical data is collected and used for quantifying the bias introduced at the representational level due to landmark uncertainty. In addition, these studies investigate the effectiveness of landmark signature design solutions based on MacEachren (1992) to communicate spatial uncertainty. 

It was assumed that in the context of map use related to wayfinding and navigation tasks, the visual implication of a landmark is most efficient when, besides its location, also its spatial uncertainty is successfully conveyed. If the map user assumes that uncertainty is an inherent feature of a map then she/he is able to incorporate these implications when coping with incomplete or inaccurate maps. Therefore, the project examines whether maps can be designed in a way that suggests uncertainty to be represented implicitly. In consequence, the uncertainty present in the VGI data would be communicated to and thus made usable for the map reader. We assumed to reveal that map users who adopt such a strategy for wayfinding tasks show higher flexibility in coping with thematic uncertainties and improved navigation performance. The findings of the project can help improving our understanding of how characteristics of VGI data influence user interaction. Ultimately, this will contribute to enhancing VGI-based map design in future navigation applications.

Effects of spatial inaccuracies on map matching
--------

In a first study, we aimed to quantify how spatial and semantic inaccuracies of landmark representations in maps affect successful matching between maps and the represented 3D environments. For this purpose, we created a virtual 3D environment and a corresponding map representation (see fig. 2). The 3D environment consisted of a long road and two T-intersections, uniformly designed buildings along the road and one unique landmark building. Random spatial inaccuracies were applied to the location of the landmark map representation along the long road. Additionally, in separate trials, landmark map pictograms were placed on the middle of the road or on the wrong side of the road. In two additional trials, the 3D environment contained two unique landmark buildings with different pictogram representations. In one trial, landmark pictograms were placed in the correct map locations. In the other trial, the locations of the two landmark pictograms in the map were switched. After each trial, participants were asked to report on the perceived match between the 3D environment and the map using a continuous scale ranging from a certain mismatch to a certain match. The center area of the scale represented uncertainty varying degrees of uncertainty concerning the match.

![Map matching task](Fig_2_-_map_matching_task.PNG)

**Fig. 2. Map matching task.** *Participants saw a virtual 3D environment and a corresponding map representation. Random spatial inaccuracies were applied to the landmark representation in the map to investigate effects of spatial inaccuracy on map matching.* 

The results demonstrate that with increasing spatial inaccuracies of the landmark representation, participants were less likely to perceive a match between the 3D environment and the map (see fig. 3). 

![Perceived match](Fig_3_-_perceived_match.png)

**Fig. 3. Perceived match.** *Larger spatial inaccuracies of landmark representations caused the perception of a mismatch between the 3D environment and its map representation.* 

Furthermore, similar to spatial inaccuracies, inaccurate semantic information of the landmark representations in the map led to the perception of a mismatch between the 3D environment and its map representations (see fig. 4). This is true for both the semantic information communicated by the landmark pictogram design (as demonstrated by switching the locations of two different landmark pictograms), as well as semantic categories provided based on separated spatial regions, e.g. different sides of a road. 

![Semantic inaccuracies](Fig_4_-_semantic_inaccuracies.png)

**Fig. 4. Semantic inaccuracies.** *Most participants rated the map representation as not matching the 3D environment when the landmark pictogram was located on the wrong side of the road, in the middle of the road or when the locations of two landmark pictograms were switched.* 

The findings of our first study demonstrate that both spatial and semantic inaccuracies of landmark inaccuracies in maps affect the ability to match a map to the represented spatial environment. This illustrates the importance to address spatial and semantic uncertainty of landmark representations in maps, as unsuccessful map matching could affect spatial orientation, wayfinding and navigation. First, difficulties during map matching can slow down or even prevent successful spatial orientation and navigation. Second, if people perceive maps as unreliable due to spatial and semantic inaccuracies and resulting difficulties during map matching, they might be reluctant to further use the maps, even if only some of the map elements are spatially or semantically inaccurate. In the subsequent studies, we addressed these issues by investigating how successful map matching can be achieved despite spatial landmark inaccuracies.

Visualizing spatial uncertainty
--------

In two consecutive studies, we investigated whether methods to visualize spatial uncertainty as suggested by MacEachren (1992) can be used to improve successful map matching in the context of spatially inaccurate landmark representations in maps. The purpose of such uncertainty visualizations is to inform the map user about potential spatial inaccuracies of map elements and thereby provide the basis for more informed decision making (cf. Padilla et al., 2014; Pang et al., 1997). Instead of perceiving a mismatch between a (partially) spatially inaccurate map and the represented space, map users could refer to other map elements for map matching if they are aware that some map elements as landmark pictograms may be spatially more or less inaccurate.

In the second study of the research project, we replicated the experiment from the first study and added three uncertainty visualization types. These visualizations included size variations of the landmark pictograms, pictogram transparency and a circular uncertainty area around the landmark pictograms. 

The third study addresses the fact that, opposed to the fully controlled minimalistic 3D environment used in the first two studies, real world environments are usually highly complex in terms of street design and other spatial elements that can be used as spatial reference points for map matching. Thus, it can be assumed that the role of single landmarks and their map representations is less crucial for map matching and spatial inaccuracies of a single landmark representation will have less effects on successful map matching. For this purpose, we replicated the previous study using 360 image recordings of real-world street scenes. The corresponding maps were obtained from OpenStreetMap and random spatial inaccuracies were applied to the map representations of a landmark visible in the 360 images. In addition to a control condition, the same uncertainty visualizations as in the previous study (size variations of the landmark pictograms, pictogram transparency and ircular uncertainty areas) were applied to the landmark map representations. 

the results of the two studies are currently being published in a peer reviewed journal and will be reported here once the publication process is completed. 

References
--------
Girres, J.‑F., & Touya, G. (2010). Quality Assessment of the French OpenStreetMap Dataset. Transactions in GIS, 14(4), 435–459. https://doi.org/10.1111/j.1467-9671.2010.01203.x

MacEachren, A. M. (1992). Visualizing Uncertain Information. Cartographic Perspectives(13), 10–19. https://doi.org/10.14714/CP13.1000

Padilla, L., Kay, M., & Hullman, J. (2014). Uncertainty Visualization. In N. Balakrishnan, T. Colton, B. Everitt, W. Piegorsch, F. Ruggeri, & J. L. Teugels (Eds.), Wiley StatsRef: Statistics Reference Online (Vol. 26, pp. 1–18). Wiley. https://doi.org/10.1002/9781118445112.stat08296

Pang, A. T., Wittenbrink, C. M., & Lodha, S. K. (1997). Approaches to Uncertainty Visualization. The Visual Computer, 13(8), 370–390.

Sieber, R. E., & Haklay, M. (2015). The epistemology(s) of volunteered geographic information: a critique. Geo: Geography and Environment, 2(2), 122–136. https://doi.org/10.1002/geo2.10

van Exel, M., Dias, E., & Fruijtier, S. (2010). The impact of crowdsourcing on spatial data qualityindicators. GIScience 2010. http://giscience2010.org/index6b70.html

Zandbergen, P. A., & Barbeau, S. J. (2011). Positional Accuracy of Assisted GPS Data from High-Sensitivity GPS-enabled Mobile Phones. Journal of Navigation, 64(3), 381–399. https://doi.org/10.1017/S0373463311000051

